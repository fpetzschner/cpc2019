# Official Repository of the Computational Psychiatry Course 2019

### Contact
Dr. Frederike Petzschner  
petzschner@biomed.ee.ethz.ch  

### Content
*	Slides: Pdf version of the slides for all lectures  
*	Code: Code and Installation Instructions for all practical sessions  
*	ReadMe  

### Download
You can download the full folder as a .zip or single files   
by clicking on the desired file in the source structure and then 'view as raw'.
