This folder contains the data, instructions, matlab functions and scripts and the slides used in the DCM tutorial of the CP course 2019.

We do not distribute SPM with this folder. In order to run, please download the latest version of SPM12 from https://www.fil.ion.ucl.ac.uk/spm/ or
from https://github.com/spm. Unzip spm, so that the spm.m is directly in the folder code/spm12 of this tutorial folder.

Follow the instructions given in CPCZurich2019_Tutorials_DCM_Instructions.pdf to run through the tutorial. 

Finally, the file DCM4fMRI_Tutorial_CPC2019_HarrisonHeinzle.pdf gives a presentation of the task and some GLM results.

Authors: 
Samuel Harrison (harrison@biomed.ee.ethz.ch)
Jakob Heinzle (heinzle@biomed.ee.ethz.ch)
